<?php

$cfg = array(
	'class' => 'DbConnection',
	'connectionString' => 'mysql:host=pudra_mysql;dbname=pudra',
	'emulatePrepare' => true,
	'username' => 'pudra_user',
	'password' => 'pudra_pass',
	'charset' => 'utf8',
	'tablePrefix' => 'cscart_',
	'pdoClass' => 'PudraPDO',

	'schemaCachingDuration' => 3600, // Кешируем схему базы

	//'schemaCacheID' => 'PUDYcache',
	//'queryCachingDuration' => 10, // Время кеширования запросов
	//'queryCachingCount' => 10,

	// 'enableProfiling'=>true, // включаем профайлер
	// 'enableParamLogging' => true, // показываем значения параметров
);
