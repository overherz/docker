<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/


if ( !defined('AREA') ) { die('Access denied'); }

/*
 * PHP options
 */

// Disable notices displaying
error_reporting(E_ALL ^ E_NOTICE);
if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
	error_reporting(error_reporting() & ~E_DEPRECATED & ~E_STRICT);
}

// Set maximum memory limit
if (PHP_INT_SIZE == 4 && (substr(ini_get('memory_limit'), 0, -1) < "48")) {
  // 32bit PHP
  @ini_set('memory_limit', '48M');
} elseif (PHP_INT_SIZE == 8 && (substr(ini_get('memory_limit'), 0, -1) < "256")) {
  // 64bit PHP
  @ini_set('memory_limit', '256M');
}



require_once(realpath(__DIR__ . '/../yii/app/inc.init.php'));

/*
 * Database connection options
 */
$config['db_host'] = 'pudra_mysql';
$config['db_name'] = 'pudra';
$config['db_user'] = 'pudra_user';
$config['db_password'] = 'pudra_pass';
$config['db_type'] = 'yii';

/*
 * Script location options
 *
 *	Example:
 *	Your url is http://www.yourcompany.com/store/cart
 *	$config['http_host'] = 'www.yourcompany.com';
 *	$config['http_path'] = '/store/cart';
 *
 *	Your secure url is https://secure.yourcompany.com/secure_dir/cart
 *	$config['https_host'] = 'secure.yourcompany.com';
 *	$config['https_path'] = '/secure_dir/cart';
 *
 */

// Host and directory where software is installed on no-secure server
$config['http_host'] = 'pudra.test';
$config['http_path'] = \Config::get('host.location.http_path');

// Host and directory where software is installed on secure server
$config['https_host'] = 'pudra.test';
$config['https_path'] = \Config::get('host.location.https_path');

/*
 * Misc options
 */
// Names of index files for administrative and customer areas
$config['admin_index'] = 'adminkA.php';
$config['customer_index'] = 'index.php';
$config['vendor_index'] = 'vendor.php';

// DEMO mode
$config['demo_mode'] = false;

// Tweaks
$config['tweaks'] = array (
	'js_compression' => false, // enables compession to reduce size of javascript files
	'check_templates' => true, // disables templates checking to improve template engine speed
	'inline_compilation' => true, // compiles nested templates in one file
	'anti_csrf' => false, // protect forms from CSRF attacks
	'disable_block_cache' => true, // used to disable block cache
	'join_css' => false, // is used to unite css files into one file
	'join_javascript' => false,
	'allow_php_in_templates' => false, // Allow to use {php} tags in templates
	'disable_localizations' => true, // Disable Localizations functionality
	'disable_google_base' => true, //Disable obsolete google base functionality
	'disable_database_cache' => true, // Отключить кеширование SQL запросов к database
);

// Cache backend
// Available backends: file, sqlite, mysql
// To use sqlite cache the "sqlite3" PHP module should be installed
$config['cache_backend'] = 'yii';

// Key for sensitive data encryption
$config['crypt_key'] = \Config::get('security.crypt_key');

// Params for social services
//apiId: 3544474 for http://pudra.ru/
//apiId: 3544538 for http://pudra.parcsis.net/
//apiId: 3544479 for http://pudra.local/
//apiId: 3630454 for http://pudra.st
$config['social'] = array (
	//'vkApiId' => 3544474 // For http://pudra.ru/
	'vkApiId' => \Yii::app()->hasComponent('socialAPI') ? \Yii::app()->socialAPI->vkontakte->appId : '',
);

$config['pudra']['feature']['composition'] = 25; // feature_id "ингридиенты ноты"
$config['pudra']['feature']['color'] = 358; // feature_id "Цвет"
$config['pudra']['is_production'] = 0; // в продуктив подключаем минифицированные css и js

/**
 * Форсируем количество продуктов на страницу в пажинаторе для Customer зоны
 */
$config['pudra']['pagination'] = array(
	'C' => array(
		'force_per_page_steps' => array(40, 80, 120),
	),
	'A' => null,
);

// shipping_id соответствующих служб доставки
$config['pudra']['shipping'] = array(
	15 => 'imlogistic',
	14 => 'pickpoint',
	11 => 'delivery',
	\Config::get('delivery.shippings.dpd_postamats.shipping_id') => 'dpd_postamats',
);
// Службы для которых контрагент с получателем меняются в 1С
$config['pudra']['1c']['shipping_russian_post'] = array(
	6  => 'rus_post',
	7  => 'ems_rus_post',
	11 => 'delivery',     // Курьерская доставка
	23 => 'ems_post_hidden',
);

$config['pudra']['product_templates'] = array(
	// Шаблоны товаров с выбором цветов
	'color' => array(
		'makeup_blesk',
		'makeup_karandash',
		'makeup_lak',
		'makeup_podvodka',
		'makeup_pomada',
		'makeup_pudra',
		'makeup_rumyana',
		'makeup_teni',
		'makeup_tonalka',
		'makeup_tush',
	),
	// Шаблоны товаров с выбором объёма
	'volume' => array(
		'parfume_with_bottles',
	),
	'care_volumes' => array(
		'care_volumes',
	),
);

// Настройки кеширования в memcached
$config['pudra']['memcached'] = \Config::get('host.cache.' . $config['cache_backend']);

// Тег, которым помечаются все записи кеша.
// Это же значение используется как префикс чтобы не путать кеш двух сайтов
// PUDP - Pudra Production
// PUDD - Pudra Developement
$config['pudra']['memcached_supertag'] = \Config::get('host.cache.super_tag', 'PUDT');

//
// Дальше специально для юнит-тестов
//

$config['pudra']['tests'] = array(
	'memcached' => $config['pudra']['memcached'],
);

$config['twg'] = array(
     'jsurl' => '/skins/pudra/customer/addons/twigmo', // путь до собранных папок js & css
     'skin' => 'pudra',
     'preset' => 'default'
 );

define('DEBUG_MODE', true);
define('DEVELOPMENT', true);
 //define('PROFILER', true);
/// define('PROFILER_SQL', true);
// define('PROFILER_FOR_ADMIN', true);