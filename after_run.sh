#!/bin/bash
docker exec -i pudra_fpm1 /bin/sh -c "pkill -f EmailSend";
docker exec -i pudra_fpm1 /bin/sh -c "pkill -f HttpRequest";
docker exec -i pudra_fpm1 /bin/sh -c "pkill -f MindboxRequest";
docker exec -i pudra_fpm1 /bin/sh -c "pkill -f SmsSend";
docker exec -i pudra_fpm1 /bin/sh -c "php /apps/pudra/etc/tools/yiic mq startConsumer --class=EmailSend >> /dev/null 2>&1 &";
docker exec -i pudra_fpm1 /bin/sh -c "php /apps/pudra/etc/tools/yiic mq startConsumer --class=HttpRequest >> /dev/null 2>&1 &";
docker exec -i pudra_fpm1 /bin/sh -c "php /apps/pudra/etc/tools/yiic mq startConsumer --class=MindboxRequest >> /dev/null 2>&1 &";
docker exec -i pudra_fpm1 /bin/sh -c "php /apps/pudra/etc/tools/yiic mq startConsumer --class=SmsSend >> /dev/null 2>&1 &";
docker exec -i pudra_node /bin/sh -c "pkill -f grunt";
docker exec -i pudra_node /bin/sh -c "cd /apps/pudra && grunt >> /dev/null 2>&1 &";