#!/bin/bash

docker exec -i pudra_fpm1 /bin/sh -c "cd /apps/pudra && composer update && cd /apps/pudra-opt && composer update";
docker exec -i pudra_node /bin/sh -c "cd /apps/pudra && npm update && cd /apps/pudra-opt && npm update";