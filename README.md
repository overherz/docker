# Docker

Full Pudra Docker Environment

## Установка докера
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
    
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
(если у вас mint или что-то другое, то правим файл /etc/apt/sources.list.d/additional-repositories.list и вписываем туда название дистрибутива ubuntu, например заменяем sylvia на xenial)   
    
sudo apt-get update
sudo apt-get install docker-ce

## Избавляемся от sudo и проблем с правами на файлы
https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user

## Импорт базы
Нужно запустить mysql контейнер
docker-compose up -d mysql
и импортировать базу 
docker exec -i pudra_mysql mysql -uroot -pjckjck --database=pudra < database.sql

## Конфиги
Для правильного запуска докера, нужно скопировать конфиги в рабочие папки
Рабочие дев-конфиги лежат в папке projects_configs

## Настройка окружения
В папке докера создаем файл .env и прописываем туда свои переменные, например PROJECT_PUDRA=D:\pudra
Если у вас Windows, обязательно вписать строчку COMPOSE_CONVERT_WINDOWS_PATHS=1

Для более быстрого запуска (только не в первый раз!) можно вписать FAST_RUN=true, тогда будут проигнорированы npm, grunt, composer на старте

## Игнорирование изменения прав в проектах
внутри проекта 
```console
git config core.filemode false
```    
глобально 
```console
git config --add --global core.filemode false
```

## Работа
Здесь и дальше, считаем что находимся в директории докера

### Шпаргалка по коммандам
Запустить все контейнеры:
```console
docker-compose up
```

Запустить все контейнеры в фоне (режим демона):
```console
docker-compose up -d
```

Посмотреть какие контейнеры запущены и их состояние:
```console
docker ps
```

Посмотреть потраченные ресурсы:
```console
docker stats
```

Подключиться к запущенному контейнеру (на примере fpm1) в консоль (внутренние имена
контейнеров можно подглядеть в выводе `docker ps`):
```console
docker exec -it pudra_fpm1 bash
```
если ругается на отсутствие bash, то скорее всего в контейнере есть sh

,здесь
 - `-it` - интерактивный режим
 - `pudra_nginx` - имя контейнера к которому подключаемся
 - `bash` - оболочка в которой будем работать
 
 
## Установка зависимостей
Происходит автоматически, но если нужно обновить вручную, то
npm - в контейнере pudra_node
composer - в контейнерах pudra_fpm1, pudra_fpm2, pudra_sphinx 